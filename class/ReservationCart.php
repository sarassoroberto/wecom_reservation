<?php
/**
 * ReservationCart
 * carrello che si forma con le reservation
 */

class ReservationCart
{
  private $prestashop_cart;
  public $product_list = [];

  private $ps_products_list;
  private $cart_reference_list;

  public $totat = 0;
  public $reservation_accept = false;
  // $cart->total_vat_exc= 0;
  public $cart_quantity = 0;

  public $total_vat_component = 0;
  public $total = 0;
  public $total_wt = 0;
  public $total_vat = 0;
  //$product_cart = $cart_products_list[0];
  public $total_reservation_value = 0;

  /**
   * viene passato il contesto per poter accedere alle varibili globali
   */
  public function __construct($context){
    $this->prestashop_cart = $context->cart;
    //$this->reservation_cart = isset($this->context->reservation_cart[$this->context->cart->id]) ?
    //echo "prestashop_cart";
    //print_r($this->prestashop_cart);
    $this->fromCartToReservation();

  }
  /**
   * valuta se ogni elemento del carrello corrisponde a una di queste condizioni
   * 01 - prodotto non nel carrello e ha non ha una reservation attiva
   *      controlla la referenza
   * 02 - prodotto non e nel carrello e ha una reservationfee
   * 03 - prodotto non e nel
   */
  public function fromCartToReservation(){
    //ottengo prodotti del ccarrello attivo
    $this->ps_products_list = $this->prestashop_cart->getProducts();
    $this->cart_reference_list = array_column($this->ps_products_list,'reference');

    foreach ($this->ps_products_list as $product_cart) {
      //$cart = [];
        $product_cart = (object) $product_cart;

        $cart_item = $this->getReservationByProduct($product_cart);
        //$cart->addItem($cart_item);
    }

    //$cart->close();
  }


  public function checkItemInCart($cart_item)
  {
      //01 - prodotto nel cartello e una reservation
    if ($this->contains($cart_item)){
      if($this->is_reservation()){
        //Aggiornare reservation cart aggiungendo la quantita del singolo item

      }else{

      }


    }else{
      //nuovo item nel carrello
    }


  }

  /**
   * controlla se un prodotto e dentro al carrello o la sua reservationfee
   *
   */
  public function contains($cart_item)
  {
    return array_search($cart_item->reference,$this->cart_reference_list);
  }



  public function is_reservation($reference){
  //echo  'sub --> '.substr($reference,0,3);
    return substr($reference,0,3) === 'RS_';
  }



  //controlla se ogni elemento del carrello deve avere una reservation
  public function addItem($cart_item){
    $this->product_list[] = $cart_item;

    //TODO: SPOSTARE logica  alla fine del ciclo.
    //$this->cart_quantity += $cart_item->product->cart_quantity;
    //$this->total += $cart_item->product->total;
    //$this->total_wt += $cart_item->product->total_wt ;

    //$this->total_vat =  $this->total_wt - $this->total ;

    // if($cart_item->reservation){
    //   $this->total_reservation_value += $cart_item->reservation->total_reservation_value;
    //
    // }


  }
  /**
  * trasformazione del carrello di prestashop
  */



  public function setTotal(){
    $this->total = $this->shipping + $this->total;
  }
  public function setShipping(){
    $this->shipping = 100;
  }
  public function setDiscount(){
    $this->discount = 1000;
  }


  public function close(){
    $this->setShipping();
    $this->setDiscount();
    $this->setTotalReservationOrder();
    $this->setTotal();
    $this->setRemainToPay();
  }

  public function setTotalReservationOrder(){
    $this->total_reservation_order = 'to do';
  }
  public function setRemainToPay(){
    $this->remain_to_pay =   $this->total
                             - $this->total_reservation_value
                             - $this->discount;
  }

  public function updateQty($item){
    $id_cart = $this->prestashop_cart->id;
    $id_product = $item->reservation->id_product;

    $id_product_attribute = $item->reservation->id_product_attribute;
    $qta = $item->product->cart_quantity;

  }



  protected function fixQuantityOfFee($item)
    {
        $id_cart = $this->prestashop_cart->id;
        $id_product = $item->reservation->id_product;
        $id_product_attribute = $item->reservation->id_product_attribute;
        $qta = $item->product->cart_quantity;

        $sql = '
            UPDATE `' . _DB_PREFIX_ . 'cart_product`
            SET `quantity` = '.(int)$qta.'
            WHERE id_cart = '.(int)$id_cart.'
                  and id_product = '.(int)$id_product.'
                  and id_product = '.(int)$id_product_attribute.'
        ';

        //bisogna controllare se il prodotto essiste
        //#select id_product,id_product_attribute from ps_cart_product
#where id_product=3

//
// UPDATE `ps_cart_product`
// 			SET `id_product`='1' WHERE  `id_cart`=6 AND `id_product`=2 AND `id_product_attribute`=10 AND `id_customization`=0 AND `id_address_delivery`=0;

//UPDATE `ps_cart_product` SET `quantity` = 5 WHERE id_cart = 177 and id_product = 11 and id_product = 50
//

        echo $sql;
        Db::getInstance()->execute($sql);
    }


  public function updateCartItem(){
    // $this->cart;
    foreach ($this->product_list as $item) {

      if($item->reservation){
        $product = $item->product;
        $reservation = $item->reservation;

        $this->fixQuantityOfFee($item);

      }
    }

  }


}


/**
 * Reservation cart item
 * ogni item dell carrello deve poter essere identificato
 */
class CartItem
{
    public $reservation;
    public $product;
    //public $reservation_value;

    public function __construct($product, $reservation){
      $this->product =  (object) $product;

      if($reservation){
        $this->reservation =  (object) $reservation;
        $this->setReservationPrice();
      }else{
          $this->reservation =  false;
      }

    }



    public function setReservationPrice(){
      //reservation price -2500
      //prezzo del prodotto 4900
      // ottengo il valore della reservation fee
      $this->reservation->reservation_value = $this->product->price + $this->reservation->price;
      $this->reservation->total_reservation_value = $this->reservation->reservation_value * $this->product->quantity;

    }


  }//cart class
