<?php

stdClass Object
(
    [product_list] => Array
        (
            [0] => stdClass Object
                (
                    [product] => stdClass Object
                        (
                            [id_product_attribute] => 46
                            [id_product] => 10
                            [cart_quantity] => 2
                            [id_shop] => 1
                            [id_customization] =>
                            [name] => e.DO 6 AXES
                            [is_virtual] => 0
                            [description_short] => ''

                            [available_now] =>
                            [available_later] =>
                            [id_category_default] => 2
                            [id_supplier] => 0
                            [id_manufacturer] => 0
                            [manufacturer_name] =>
                            [on_sale] => 0
                            [ecotax] => 0.000000
                            [additional_shipping_cost] => 0.00
                            [available_for_order] => 1
                            [show_price] => 1
                            [price] => 4900
                            [active] => 1
                            [unity] =>
                            [unit_price_ratio] => 0.000000
                            [quantity_available] => 93
                            [width] => 0.000000
                            [height] => 0.000000
                            [depth] => 0.000000
                            [out_of_stock] => 2
                            [weight] => 0
                            [available_date] => 0000-00-00
                            [date_add] => 2017-10-13 13:20:13
                            [date_upd] => 2017-11-15 07:56:23
                            [quantity] => 2
                            [link_rewrite] => edo-6-axis
                            [category] => home
                            [unique_id] => 0000000010000000004600
                            [id_address_delivery] => 0
                            [advanced_stock_management] => 0
                            [supplier_reference] =>
                            [customization_quantity] =>
                            [price_attribute] => 0.000000
                            [ecotax_attr] => 0.000000
                            [reference] => CR82428700_KIT
                            [weight_attribute] => 0
                            [ean13] =>
                            [isbn] =>
                            [upc] =>
                            [minimal_quantity] => 1
                            [wholesale_price] => 0.000000
                            [id_image] => 10-24
                            [legend] =>
                            [reduction_type] => 0
                            [is_gift] =>
                            [reduction] => 0
                            [price_without_reduction] => 5978
                            [specific_prices] => Array
                                (
                                )

                            [stock_quantity] => 93
                            [price_with_reduction] => 5978
                            [price_with_reduction_without_tax] => 4900
                            [total] => 9800
                            [total_wt] => 11956
                            [price_wt] => 5978
                            [reduction_applies] =>
                            [quantity_discount_applies] =>
                            [allow_oosp] => 0
                            [features] => Array
                                (
                                )

                            [attributes] => Your Mounting Option : Mounting Kit
                            [attributes_small] => Mounting Kit
                            [rate] => 22
                            [tax_name] => IVA IT 22%
                        )

                    [is_reservation] =>
                )

            [1] => stdClass Object
                (
                    [product] => stdClass Object
                        (
                            [id_product_attribute] => 48
                            [id_product] => 11
                            [cart_quantity] => 2
                            [id_shop] => 1
                            [id_customization] =>
                            [name] => e.DO 4 AXES
                            [is_virtual] => 0
                            [description_short] =>
The 4-axis version of e.DO has a total of 4 articulated axes that interface and communicate between themselves in a modular and independent manner. Axis 4 and Axis 6 are static.


Each motorized unit has an autonomous mechanical and electronic control that can be configured as needed by the operator, allowing e.DO to pick up, lift and move symmetrical pieces in a horizontal or vertical manner.


                            [available_now] =>
                            [available_later] =>
                            [id_category_default] => 2
                            [id_supplier] => 0
                            [id_manufacturer] => 0
                            [manufacturer_name] =>
                            [on_sale] => 0
                            [ecotax] => 0.000000
                            [additional_shipping_cost] => 0.00
                            [available_for_order] => 1
                            [show_price] => 1
                            [price] => 3500
                            [active] => 1
                            [unity] =>
                            [unit_price_ratio] => 0.000000
                            [quantity_available] => 92
                            [width] => 0.000000
                            [height] => 0.000000
                            [depth] => 0.000000
                            [out_of_stock] => 2
                            [weight] => 0
                            [available_date] => 0000-00-00
                            [date_add] => 2017-10-13 13:48:41
                            [date_upd] => 2017-12-15 13:15:32
                            [quantity] => 2
                            [link_rewrite] => edo-4-axis
                            [category] => home
                            [unique_id] => 0000000011000000004800
                            [id_address_delivery] => 0
                            [advanced_stock_management] => 0
                            [supplier_reference] =>
                            [customization_quantity] =>
                            [price_attribute] => 0.000000
                            [ecotax_attr] => 0.000000
                            [reference] => CR82431700_KIT
                            [weight_attribute] => 0
                            [ean13] =>
                            [isbn] =>
                            [upc] =>
                            [minimal_quantity] => 1
                            [wholesale_price] => 0.000000
                            [id_image] => 11-31
                            [legend] =>
                            [reduction_type] => 0
                            [is_gift] =>
                            [reduction] => 0
                            [price_without_reduction] => 4270
                            [specific_prices] => Array
                                (
                                )

                            [stock_quantity] => 92
                            [price_with_reduction] => 4270
                            [price_with_reduction_without_tax] => 3500
                            [total] => 7000
                            [total_wt] => 8540
                            [price_wt] => 4270
                            [reduction_applies] =>
                            [quantity_discount_applies] =>
                            [allow_oosp] => 0
                            [features] => Array
                                (
                                )

                            [attributes] => Your Mounting Option : Mounting Kit
                            [attributes_small] => Mounting Kit
                            [rate] => 22
                            [tax_name] => IVA IT 22%
                        )

                    [is_reservation] => 1
                    [reservation] => stdClass Object
                        (
                            [id_product_attribute] => 50
                            [id_product] => 11
                            [reference] => RS_CR82431700_KIT
                            [supplier_reference] =>
                            [location] =>
                            [ean13] =>
                            [isbn] =>
                            [upc] =>
                            [wholesale_price] => 0.000000
                            [price] => -3250.000000
                            [ecotax] => 0.000000
                            [quantity] => 100
                            [weight] => 0.000000
                            [unit_price_impact] => 0.000000
                            [default_on] =>
                            [minimal_quantity] => 1
                            [available_date] => 0000-00-00
                            [id_shop] => 1
                            [id_attribute_group] => 5
                            [is_color_group] => 0
                            [group_name] => reservation fee
                            [attribute_name] => SI
                            [id_attribute] => 27
                        )

                )

            [2] => stdClass Object
                (
                    [product] => stdClass Object
                        (
                            [id_product_attribute] => 49
                            [id_product] => 11
                            [cart_quantity] => 1
                            [id_shop] => 1
                            [id_customization] =>
                            [name] => e.DO 4 AXES
                            [is_virtual] => 0
                            [description_short] =>
The 4-axis version of e.DO has a total of 4 articulated axes that interface and communicate between themselves in a modular and independent manner. Axis 4 and Axis 6 are static.


Each motorized unit has an autonomous mechanical and electronic control that can be configured as needed by the operator, allowing e.DO to pick up, lift and move symmetrical pieces in a horizontal or vertical manner.


                            [available_now] =>
                            [available_later] =>
                            [id_category_default] => 2
                            [id_supplier] => 0
                            [id_manufacturer] => 0
                            [manufacturer_name] =>
                            [on_sale] => 0
                            [ecotax] => 0.000000
                            [additional_shipping_cost] => 0.00
                            [available_for_order] => 1
                            [show_price] => 1
                            [price] => 4000
                            [active] => 1
                            [unity] =>
                            [unit_price_ratio] => 0.000000
                            [quantity_available] => 100
                            [width] => 0.000000
                            [height] => 0.000000
                            [depth] => 0.000000
                            [out_of_stock] => 2
                            [weight] => 0
                            [available_date] => 0000-00-00
                            [date_add] => 2017-10-13 13:48:41
                            [date_upd] => 2017-12-15 13:15:32
                            [quantity] => 1
                            [link_rewrite] => edo-4-axis
                            [category] => home
                            [unique_id] => 0000000011000000004900
                            [id_address_delivery] => 0
                            [advanced_stock_management] => 0
                            [supplier_reference] =>
                            [customization_quantity] =>
                            [price_attribute] => 500.000000
                            [ecotax_attr] => 0.000000
                            [reference] => CR82431700_ARM
                            [weight_attribute] => 0
                            [ean13] =>
                            [isbn] =>
                            [upc] =>
                            [minimal_quantity] => 1
                            [wholesale_price] => 0.000000
                            [id_image] => 11-31
                            [legend] =>
                            [reduction_type] => 0
                            [is_gift] =>
                            [reduction] => 0
                            [price_without_reduction] => 4880
                            [specific_prices] => Array
                                (
                                )

                            [stock_quantity] => 100
                            [price_with_reduction] => 4880
                            [price_with_reduction_without_tax] => 4000
                            [total] => 4000
                            [total_wt] => 4880
                            [price_wt] => 4880
                            [reduction_applies] =>
                            [quantity_discount_applies] =>
                            [allow_oosp] => 0
                            [features] => Array
                                (
                                )

                            [attributes] => Your Mounting Option : Assembled
                            [attributes_small] => Assembled
                            [rate] => 22
                            [tax_name] => IVA IT 22%
                        )

                    [is_reservation] => 1
                    [reservation] => stdClass Object
                        (
                            [id_product_attribute] => 51
                            [id_product] => 11
                            [reference] => RS_CR82431700_ARM
                            [supplier_reference] =>
                            [location] =>
                            [ean13] =>
                            [isbn] =>
                            [upc] =>
                            [wholesale_price] => 0.000000
                            [price] => -3250.000000
                            [ecotax] => 0.000000
                            [quantity] => 100
                            [weight] => 0.000000
                            [unit_price_impact] => 0.000000
                            [default_on] =>
                            [minimal_quantity] => 1
                            [available_date] => 0000-00-00
                            [id_shop] => 1
                            [id_attribute_group] => 5
                            [is_color_group] => 0
                            [group_name] => reservation fee
                            [attribute_name] => SI
                            [id_attribute] => 27
                        )

                )

        )

    [totat] => 0
    [total_qta] => 0
    [total_vat_component] => 0
    [total] => 0
    [total_wt] => 0
