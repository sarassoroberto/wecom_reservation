<?php

stdClass Object
(
    [product_list] => Array
        (
            [0] => stdClass Object
                (
                    [product] => stdClass Object
                        (
                            [id_product_attribute] => 46
                            [id_product] => 10
                            [cart_quantity] => 2

                            [name] => e.DO 6 AXES

                            [available_for_order] => 1
                            [price] => 4900
                            [active] => 1
                            [unit_price_ratio] => 0.000000
                            [quantity] => 2
                            [reference] => CR82428700_KIT
                            [id_image] => 10-24
                            [price_without_reduction] => 5978
                            [stock_quantity] => 93
                            [price_with_reduction] => 5978
                            [price_with_reduction_without_tax] => 4900
                            [total] => 9800
                            [total_wt] => 11956
                            [price_wt] => 5978
                            [attributes] => Your Mounting Option : Mounting Kit
                            [attributes_small] => Mounting Kit
                            [rate] => 22
                            [tax_name] => IVA IT 22%
                        )

                )



                    [is_reservation] => 1
                    [reservation] => stdClass Object
                        (
                            [id_product_attribute] => 50
                            [id_product] => 11
                            [reference] => RS_CR82431700_KIT
                            [wholesale_price] => 0.000000
                            [price] => -3250.000000
                            [id_attribute_group] => 5
                            [id_attribute] => 27
                        )

                )


        )

    [totat] => 0
    [total_qta] => 0
    [total_vat_component] => 0
    [total] => 0
    [total_wt] => 0
