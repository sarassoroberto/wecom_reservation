<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

// require_once _PS_MODULE_DIR_ . Wecom_reservation::BASE_DIR'/includer.php';

function debug($mixed,$label='debug'){
    return $html .= '<pre>'.print_r($mixed,true).'</pre>';
}

class Wecom_reservation extends Module
{
    const PREFIX = 'bestkit_resfee_';
    const BASE_DIR = 'wecom_reservation';

    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'wecom_reservation';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'wecom';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('wecom reservation');
        $this->description = $this->l('descrizione del modulo');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    protected $_hooks = array(
        'header',
        'validateOrder',
        'displayPaymentTop',
        'orderConfirmation',
        'displayPDFInvoice',
        'displayPDFDeliverySlip',

        // cart hook
        'displayShoppingCartFooter',
        'displayShoppingCart',
        'displayReassurance',
        'displayExpressCheckout',


        'displayAdminOrder',
        'actionEmailAddAfterContent',
        'actionGetExtraMailTemplateVars',
        'actionValidateOrder',
        'displayAdminProductsExtra',
        'actionProductUpdate',
        'displayRightColumnProduct',
        'displayTopColumn',
        'customerAccount',
        'moduleRoutes',

        'actionCartSave',
        'actionCartUpdateQuantityBefore',
    );

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('WECOM_RESERVATION_LIVE_MODE', false);

        include(dirname(__FILE__).'/sql/install.php');


    }

    protected function setHook()
    {
      foreach ($this->_hooks as $hook) {
              if (!$this->registerHook($hook)) {
                  return FALSE;
              }
          }

    }

    public function uninstall()
    {
        Configuration::deleteByName('WECOM_RESERVATION_LIVE_MODE');

        include(dirname(__FILE__).'/sql/uninstall.php');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function _getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitWecom_reservationModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    public function getContent()
    {
        $this->setHook();
      //11 12
        $html = '';
        /**
         * If values have been submitted in the form, process.
         */
         // load product object
        $product = new Product (11, $this->context->language->id);
        $combinations = $product->getAttributeCombinations($this->context->language->id);

        // 11-50-edo-4-axis.html#/
        //25-montaggio-mounting_kit/27-reservation_fee-si

          // get the product combinations data
          // create array combinations with key = id_product
        //$combinations[$product->id] = $product->getAttributeCombinations($this->context->language->id);
        $html .= debug($combinations);

        $html .= '';
        return $html;
    }



    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitWecom_reservationModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Live mode'),
                        'name' => 'WECOM_RESERVATION_LIVE_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in live mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Enter a valid email address'),
                        'name' => 'WECOM_RESERVATION_ACCOUNT_EMAIL',
                        'label' => $this->l('Email'),
                    ),
                    array(
                        'type' => 'password',
                        'name' => 'WECOM_RESERVATION_ACCOUNT_PASSWORD',
                        'label' => $this->l('Password'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'WECOM_RESERVATION_LIVE_MODE' => Configuration::get('WECOM_RESERVATION_LIVE_MODE', true),
            'WECOM_RESERVATION_ACCOUNT_EMAIL' => Configuration::get('WECOM_RESERVATION_ACCOUNT_EMAIL', 'contact@prestashop.com'),
            'WECOM_RESERVATION_ACCOUNT_PASSWORD' => Configuration::get('WECOM_RESERVATION_ACCOUNT_PASSWORD', null),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    //NOTE hook da inserire  - quando si conferma ordine ???
    public function hookActionValidateOrder($params){
          return '<h5 class="debug">'.__FUNCTION__.'</h5>';
    }

    // note hook di reservation kit
    public function hookActionCartSave($params){
          return '<h5 class="debug">'.__FUNCTION__.'</h5>';
    }

    public function hookActionAdminControllerSetMedia()
    {
           return '<h5 class="debug">'.__FUNCTION__.'</h5>';
    }


  public function hookDisplayShoppingCartFooter()
  {
    $cart_std = $this->context->cart;
    $cart_std_products_list = $cart_std->getProducts();

    $html = '';
    $cart = new stdClass();
    $cart->product_list = [];
    $cart->totat = 0;
    // $cart->total_vat_exc= 0;
    $cart->cart_quantity = 0;
    $cart->total_vat_component = 0;
    $cart->total = 0;
    $cart->total_wt = 0;
    //$product_cart = $cart_products_list[0];
    $cart->total_reservation_value = 0;
    foreach ($cart_std_products_list as $product_cart) {
      # code...
        $cart_item = $this->getReservationByProduct($product_cart);
        $cart->product_list[] = $cart_item;


        $cart->cart_quantity += $cart_item->product->cart_quantity;
        $cart->total += $cart_item->product->total;
        if($cart_item->reservation){
          $cart->total_reservation_value += $cart_item->reservation->total_reservation_value;

        }

    }
    $cart->shipping = 'to do';
    $this->context->smarty->assign('cart', $cart);
  //  $this->context->smarty->assign('cart', $cart);
    //$this->context->smarty->assign('combinations', $combinations);
    $html = $this->context->smarty->fetch($this->local_path.'views/templates/front/recap-table.tpl');
    $html .= $this->context->smarty->fetch($this->local_path.'views/templates/front/cart_footer.tpl');
    // $html .= $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

    return $html;
    //return '<pre>'.print_r($cart,TRUE).'</pre>';
  }



  public function hookDisplayReassurance()
  {
    return '<h5 class="debug">'.__FUNCTION__.'</h5>';
  }

  public function hookDisplayShoppingCart()
  {
    return '<h5 class="debug">'.__FUNCTION__.'</h5>';
  }

  protected function getReservationByProduct($product_cart){

    //$cart = [];
    $product_cart = (object) $product_cart;
    $product_id = $product_cart->id_product;
    //oggetto prodotto
    $ps_product = new Product($product_id);
    $reservation_reference = 'RS_'.$product_cart->reference;

    //trova la combinazione con la reference RS_ + reference del prodotto
    $combinations_reference = $this->filterCombinationsByReference($reservation_reference,$ps_product);

    $item = new CartItem($product_cart,$combinations_reference);
    return $item;
  }

  private function filterCombinationsByReference($reference,$ps_product,$is_unique=true){
    $res = [];
    $combinations =  $ps_product->getAttributeCombinations($this->context->language->id);
    foreach ($combinations as $combination) {
      $combination = (object) $combination;
      if($reference ===  $combination->reference){
        if($is_unique){
          $res = $combination;
        }else{
          $res[] = $combination;
        }
      }
    }
    return $res;
  }

  public function getShipping(){
    return 666;
  }
}



/**
 * Reservation cart item
 */
class CartItem
{
    public $reservation;
    public $product;
    //public $reservation_value;

    public function __construct($product, $reservation){
      $this->product =  (object) $product;

      if($reservation){
        $this->reservation =  (object) $reservation;
        $this->setReservationPrice();
      }else{
          $this->reservation =  false;
      }

    }

    public function setReservationPrice(){
      //reservation price -2500
      //prezzo del prodotto 4900
      // ottengo il valore della reservation fee
      $this->reservation->reservation_value = $this->product->price + $this->reservation->price;
      $this->reservation->total_reservation_value = $this->reservation->reservation_value * $this->product->quantity;

    }
}
