<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

// require_once _PS_MODULE_DIR_ . Wecom_reservation::BASE_DIR'/includer.php';

function debug($mixed,$label='debug'){
    return $html .= '<pre>'.print_r($mixed,true).'</pre>';
}

class Wecom_reservation extends Module
{
    const PREFIX = 'bestkit_resfee_';
    const BASE_DIR = 'wecom_reservation';

    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'wecom_reservation';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'wecom';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('wecom reservation');
        $this->description = $this->l('descrizione del modulo');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);

          include(dirname(__FILE__).'/class/ReservationCart.php');
    }

    protected $_hooks = array(
        'header',
        'validateOrder',
        'displayPaymentTop',
        'orderConfirmation',
        'displayPDFInvoice',
        'displayPDFDeliverySlip',

        // cart hook
        'displayShoppingCartFooter',
        'displayShoppingCart',
        'displayReassurance',
        'displayExpressCheckout',


        'displayAdminOrder',
        'actionEmailAddAfterContent',
        'actionGetExtraMailTemplateVars',
        'actionValidateOrder',
        'displayAdminProductsExtra',
        'actionProductUpdate',
        'displayRightColumnProduct',
        'displayTopColumn',
        'customerAccount',
        'moduleRoutes',

        'actionCartSave',
        'actionCartUpdateQuantityBefore',

        'displayCheckoutSummaryFooter'
    );

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('WECOM_RESERVATION_LIVE_MODE', false);

        include(dirname(__FILE__).'/sql/install.php');


    }

    protected function setHook()
    {
      foreach ($this->_hooks as $hook) {
              if (!$this->registerHook($hook)) {
                  return FALSE;
              }
          }

    }

    public function uninstall()
    {
        Configuration::deleteByName('WECOM_RESERVATION_LIVE_MODE');

        include(dirname(__FILE__).'/sql/uninstall.php');

        return parent::uninstall();
    }



    public function getContent()
    {
        $this->setHook();
      //11 12
        $html = '';
        $html .= '';
        return $html;
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    //NOTE hook da inserire  - quando si conferma ordine ???
    public function hookActionValidateOrder($params){
          return '<h5 class="debug">'.__FUNCTION__.'</h5>';
    }

    // note hook di reservation kit
    public function hookActionCartSave($params){
          return '<h5 class="debug">'.__FUNCTION__.'</h5>';
    }

    public function hookActionAdminControllerSetMedia()
    {
           return '<h5 class="debug">'.__FUNCTION__.'</h5>';
    }


    public function hookDisplayShoppingCartFooter()
    {
     $html = '<h5 class="hook-debug">'.__FUNCTION__.'</h5>';



     $reservation = Tools::getValue('reservation');

     //$cart =  $this->getReservationCart();
     //$this->fillCartVar();
     $reservationCart = $this->getReservationCart();
     $reservationCart->updateCartItem();

     $this->context->smarty->assign('cart', $reservationCart);
     $html .= $this->context->smarty->fetch($this->local_path.'views/templates/front/cart_footer.tpl');
     $html .= $this->context->smarty->fetch($this->local_path.'views/templates/front/ShoppingCartFooter.tpl');

     //$this->context->smarty->assign('res_cart', $cart);

     return $html;
    }
  public function hookDisplayCheckoutSummaryFooter(){
       //return '<h5>'.__FUNCTION__.'</h5>';
       $cart =  $this->getReservationCart();

       $html='';
       $this->context->smarty->assign('res_cart', $cart);

      $html = $this->context->smarty->fetch($this->local_path.'views/templates/front/recap-table.tpl');
      //$html = $this->context->smarty->fetch($this->local_path.'views/templates/front/cart_footer.tpl');
      //$html .= $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');
      return $html;

  }


  public function getReservationCart(){
    //$cart_std = $this->context->cart;
    $html = '';
    if(isset($_SESSION['RS_CART'])){
      $cart = $_SESSION['RS_CART'];
    }else{
      $cart = new ReservationCart($this->context);
    }
    return $cart;
  }

  public function save()
  {
    if(isset($_SESSION['RS_CART'])){
      $_SESSION['RS_CART'] = $this;
    }else{
      $_SESSION['RS_CART'] = [];
      $_SESSION['RS_CART'] = $this;
    }
  }

  //compila il carrello a seconda dei prodotti che ci sono
  public function fillCartVar(){
    //ottenere iformazioni sulle reservationfee
    // 01 - getReservationFeeValues
    $reservationCart = $this->getReservationCart();
    $reservationCart->updateCartItem();
  }
  /**
   * ottiene la reservation di un determinato prodotto
   * @param $product_cart Array di una riga del carrello
   */
  protected function getReservationByProduct($product_cart){

      //$cart = [];
      //$product_cart = (object) $product_cart;
      //$product_id = $product_cart->id_product;

      if($this->is_reservation($product_cart->reference)){
        //prodotto cin RS_ deve essere ignorato
        //va ignorate
        echo $product_cart->reference.'- si ';
        //controllare se il prodotto e gia nel carrello

      }else{
        echo $product_cart->reference.'prodotto normale ';
        if($this->filterCombinationsByReference($product_cart->reference,$product_cart->product_id)){
          //trovata una reservation
          //
        }else{
          //no reservation
        }

        //trovare la reservation`
      }

      //oggetto prodotto
    //  $ps_product = new Product($product_id);

      //se il prodotto puo avere una reservation allora esiste una combinazione con
      //la reference pari a RS_
    //  $reservation_reference = 'RS_'.$product_cart->reference;

      //trova la combinazione con la reference RS_ + reference del prodotto
    //  $combinations_reference = $this->filterCombinationsByReference($reservation_reference,$ps_product);

    //  $item = new CartItem($product_cart,$combinations_reference);
      return $item;
    }

  /**
  * trova tra le combinazioni una referenza ben precisa
  * @param reference la refereza da trovare
  * @param ps_product Prestasho Product di cui bisogna trovare  le combinazioni
  */
  private function filterCombinationsByReference($reference,$ps_product,$is_unique=true){
      $res = [];
      $combinations =  $ps_product->getAttributeCombinations($this->context->language->id);
      foreach ($combinations as $combination) {
        $combination = (object) $combination;
        if($reference ===  $combination->reference){
          if($is_unique){
            $res = $combination;
          }else{
            $res[] = $combination;
          }
        }
      }
      return $res;
    }



  public function hookDisplayReassurance()
  {
    return '<h5 class="debug">'.__FUNCTION__.'</h5>';
  }

  public function hookDisplayShoppingCart()
  {
    return '<h5 class="debug">'.__FUNCTION__.'</h5>';
  }

}
