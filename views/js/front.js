/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2017 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */


var weecom_reservation = {
	cartbtn: $('.checkout.cart-detailed-actions.card-block a'),
	fee_action: document.getElementById('weecom_reservation_action'),

	init: function () {

		weecom_reservation.saveRFee();

		weecom_reservation.cartbtn.on('click', weecom_reservation.checkreservation);
	},
	//impedisce di andare avanti nel processo di pagamento se non si conferma la reservation
	checkreservation: function (event) {
		event.preventDefault();
		//reservation_cheked   = weecom_reservation.fee_action.prop('checked');
		//console.log("ciccio ",reservation_cheked,this.href);

		if (reservation_cheked) {
			location.href = this.href
		} else {
			alert('Please select the reservation fee');
			//$('#reservation_modal').modal('show')
		}
	},
	saveRFee: function () {
		weecom_reservation.fee_action.addEventListener('change', function (e) {
			console.log('saveFee');

      location.href  = location.href + '&reservation=true';

			// $.ajax({
			// 	type: 'POST',
			// 	headers: {
			// 		"cache-control": "no-cache"
			// 	},
			// 	url: rfee_ajax_controller,
			// 	async: true,
			// 	cache: false,
			// 	dataType: "json",
			// 	//data: 'use='+($(this).prop('checked') == false ? 0: 1)+'&action=saveCartFlag' ,
			// 	data: 'use=' + ($(this).prop('checked') == false ? 0 : 1) + '&action=saveCartFlag',
			// 	success: function (jsonData) {
			// 		location.reload();
			// 	},
			// 	error: function (XMLHttpRequest, textStatus, errorThrown) {
			// 		if (textStatus !== 'abort') {
			// 			error = "TECHNICAL ERROR: unable to send information about your gift \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus;
			// 			bestkit_gifts.showFancyError(error);
			// 		}
			// 	}
			// }); //end ajax



    }) //onchange
	},


} //weecom_reservation

$(document).ready(function () {
	weecom_reservation.init();
});
