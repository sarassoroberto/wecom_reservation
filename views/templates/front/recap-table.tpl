<div class="recap-box">
   <table class="recap-table table" border="1">
      <colgroup>
         <col style="width: 368px">
         <col style="width: 71px;text-align:center;">
         <col style="width: 113px">
      </colgroup>
      <tbody>
         <tr>
            <th colspan="3">
                <h3>Order</h3>
            </th>
         </tr>
         <tr>
            <th>Product</th>
            <th>Q.ta</th>
            <th>Price (no VAT)</th>
         </tr>
         <!-- loop dei prodotti -->
         {foreach $res_cart->product_list as $item}


              <tr>

                 <td>
                    <b>{$item->product->name}</b>
                   <br>
                   <p>
                     {$item->product->attributes} <br>
                     {$item->product->reference}
                     <b>({$item->product->id_product})</b>
                   </p>

                    {if $item->reservation}
                     prodotto con reservation di {$item->reservation->total_reservation_value}
                    {/if}
                 </td>
                 <td>
                   {$item->product->cart_quantity}
                </td>
                 <td>
                    {* {$item->product->price_with_reduction_without_tax} *}
                    {$item->product->total}
                </td>
              </tr>
         {/foreach}
         <tr>
           <td class="separator" colspan="3">-</td>
         </tr>

         <!-- fine loop prodotti -->
         <tr>
            <td><b>Shipping fee</b></td>
            <td>&nbsp;</td>
            <td>{$res_cart->shipping}</td>
         </tr>
         <tr>
            <td><b>VAT (product VAT)</b></td>
            <td>&nbsp;</td>
            <td>{$res_cart->total_vat}</td>
         </tr>
         <!-- totale importo dei prodotti -->
      <tfoot>
         <tr class="total-row">
            <th>total   </th>
            <th>{$res_cart->cart_quantity}</th>
            <th>
            {$res_cart->total}
          </th>
         </tr>
         <tr class="total-row-fee">
            <th>total reservation order  </th>
            <th></th>
            <th>{$res_cart->total_reservation_order}</th>

         </tr>
      </tfoot>
         <tr>
            <td colspan="3">
               <h3>Reservation and Discount</h3>
            </td>
         </tr>
         <tr>
            <td>Minus Reservatin fee</td>
            <td>&nbsp;</td>
            <td>{$res_cart->total_reservation_value}</td>
         </tr>
         <tr>
            <td>Minus Discount voucer</td>
            <td>&nbsp;</td>
            <td>{$res_cart->discount}</td>
         </tr>

         <tr>
            <th>Remaing balance to be paid upon shipment</th>
            <th>&nbsp;</th>
            <th>{$res_cart->remain_to_pay}</th>
         </tr>

      </tbody>
   </table>
</div>
